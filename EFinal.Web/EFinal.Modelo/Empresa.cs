﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFinal.Modelo
{
   public  class Empresa
    {
        [Key]
        public string StrRUC { get; set; }
        public string StrRazonSocial { get; set; }

        public string StrDireccion { get; set; }

        public string StrDepartamento { get; set; }
        public string StrProvincia { get; set; }
        public string StrDistrito { get; set; }
        public string StrRubro { get; set; }
        public Boolean BitEstado { get; set; }

        public string StrUsuarioDuenio { get; set; }

        public virtual List<Factura> Facturas { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFinal.Modelo
{
   public class Factura
    {
        public int IntIdRegistro { get; set; }
        public string StrSerie { get; set; }
        public string StrNumero { get; set; }

        public DateTime DtFechaEmision { get; set; }
        public DateTime DtFechaVcto { get; set; }

        public DateTime DtFechaCobro { get; set; }

        public decimal DcTotal { get; set; }
        public decimal DcImpuesto { get; set; }

        public virtual Empresa Empresa { get; set; }
        //public string StrRUC_Empresa { get; set; }
        public string StrNombreArchivoFactura { get; set; }
       

        public Boolean BitEstado { get; set; } 
    }
}

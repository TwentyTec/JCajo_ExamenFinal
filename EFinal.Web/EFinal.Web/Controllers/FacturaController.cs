﻿using EFinal.Web.Comun;
using EFinal.Web.Funcionalidades.AnularEmpresa;
using EFinal.Web.Funcionalidades.AnularFactura;
using EFinal.Web.Funcionalidades.EditarFactura;
using EFinal.Web.Funcionalidades.ListarFactura;
using EFinal.Web.Funcionalidades.RegistrarFactura;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EFinal.Web.Controllers
{
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Listar(string ruc)
        {
            if (ruc == null)
            {
                ruc = User.Identity.Name;            
            }

            using (var listarEmpresas = new ListarFacturaHandler())
            {
                return View(new FiltrarFacturaViewModel() { Filtro = ruc, Facturas = listarEmpresas.Ejecutar(ruc) });
            }
        }

        public PartialViewResult FiltrarporUsuario(string ruc)
        {
            using (var listaPermisos = new ListarFacturaHandler())
            {
                return PartialView("_LstFacturasEncontradas", listaPermisos.Ejecutar(ruc));
            }
        }
      

        [HttpGet]
        public ActionResult Registrar(string ruc)
        {
            RegistrarFacturaViewModel vista = new RegistrarFacturaViewModel();
            vista.StrRUC = ruc;
            return View(vista);
        }

        [HttpGet]
        public ActionResult Editar(int id)
        {
            using (var buscarPermiso = new ObtenerFacturaHandler())
            {
                return View(buscarPermiso.Ejecutar(id));
            }
        }

        [HttpPost]
        public ActionResult Editar(EditarFacturaViewModel modelo, HttpPostedFileBase image)
        {       
            string ruta = Server.MapPath("~/imagenes/Albums");
            if (!ModelState.IsValid) return View(modelo);          
            using (var registrarPermiso = new EditarFacturaHandler())
            {
                try
                {
                    Imagen imagen = ObtenerImagen(image, ruta);
                    registrarPermiso.Ejecutar(modelo, imagen);
                    return RedirectToAction("Listar", new { ruc = modelo.StrRUC });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }


        private Imagen ObtenerImagen(HttpPostedFileBase image, string ruta)
        {
            if (image == null) return null;
            var stream = new MemoryStream();

            image.InputStream.CopyTo(stream);
            return new Imagen(
                stream,
                image.FileName,
                image.ContentType,
                ruta);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registrar(RegistrarFacturaViewModel modelo, HttpPostedFileBase image)
        {
            string ruta = Server.MapPath("~/imagenes/Albums");
            if (!ModelState.IsValid) return View(modelo);

            using (var buscarFactura = new ObtenerFacturaHandler())
            {
                if (buscarFactura.ExisteFacturaSerieNumero(modelo.StrSerie,modelo.StrNumero) == true)
                {
                    ModelState.AddModelError("", "La serie y número ya está registrada");
                    return View(modelo);
                }
            }

            using (var registrarPermiso = new RegistrarFacturaHandler())
            {
                try
                {
                    Imagen imagen = ObtenerImagen(image, ruta);
                    registrarPermiso.Ejecutar(modelo, imagen);
                    return RedirectToAction("Listar",new { ruc =modelo.StrRUC});
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }


        [HttpGet]
        public ActionResult Eliminar(int id)
        {
            using (var buscarPermiso = new AnularFacturaHandler())
            {
                return View(buscarPermiso.ObtenerFacturaAnular(id));
            }
        }

        //[HttpPost]
        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {            
            using (var actualizarpermiso = new AnularFacturaHandler())
            {
                try
                {
                    actualizarpermiso.Ejecutar(id);
                    return RedirectToAction("Listar");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return RedirectToAction("Listar");
                }
            }
        }

    }
}
﻿using EFinal.Web.Funcionalidades.AnularEmpresa;
using EFinal.Web.Funcionalidades.EditarEmpresa;
using EFinal.Web.Funcionalidades.ListarEmpresas;
using EFinal.Web.Funcionalidades.RegistrarEmpresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace EFinal.Web.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Listar(string filtro)
        {
            filtro = User.Identity.Name;
            using (var listarEmpresas = new ListarEmpresaHandler())
            {
                return View(new FiltrarEmpresasViewModel() { Filtro = filtro, Empresas = listarEmpresas.Ejecutar(filtro) });
            }
        }

        public PartialViewResult FiltrarporUsuario(string filtro)
        {
            using (var listaPermisos = new ListarEmpresaHandler())
            {
                return PartialView("_LstEmpresasEncontradas", listaPermisos.Ejecutar(filtro));
            }
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new RegistrarEmpresaViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registrar(RegistrarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            modelo.StrUsuarioDuenio = User.Identity.Name;

            using (var buscarEmpresa = new ObtenerEmpresaHandler())
            {
                if (buscarEmpresa.ExisteEmpresa(modelo.StrRUC) == true)
                {
                    ModelState.AddModelError("", "El RUC YA ESTÁ REGISTRADO");
                    return View(modelo);
                }
            }

            using (var registrarPermiso = new RegistrarEmpresaHandler())
            {
                try
                {
                    registrarPermiso.Ejecutar(modelo);
                    return RedirectToAction("Listar");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }

        [HttpGet]
        public PartialViewResult Editar(string id)
        {
            using (var buscarPermiso = new ObtenerEmpresaHandler())
            {
                return PartialView("_Editar", buscarPermiso.Ejecutar(id));               
            }
        }

        [HttpPost]
        public ActionResult Editar(EditarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            using (var actualizarpermiso = new EditarEmpresaHandler())
            {
                try
                {
                    actualizarpermiso.Ejecutar(modelo);
                    return RedirectToAction("Listar");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }


        [HttpGet]
        public PartialViewResult Eliminar(string id)
        {
            using (var buscarPermiso = new ObtenerEmpresaHandler())
            {
                return PartialView("_Eliminar", buscarPermiso.Ejecutar(id));
            }
        }

        [HttpPost]
        public ActionResult EliminarConfirmacion(EditarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            using (var actualizarpermiso = new AnularEmpresaHandler())
            {
                try
                {
                    if (actualizarpermiso.ExisteFacturasEmpresa(modelo.StrRUC) == true)
                    {
                        ModelState.AddModelError("", "EXISTEN FACTURAS ASOCIADAS, NO SE PUEDE ELIMINAR");
                        //return RedirectToAction("Listar");
                        return View(modelo);
                    }
                    else
                    { 
                    actualizarpermiso.Ejecutar(modelo.StrRUC);
                    return RedirectToAction("Listar");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }
            }
        }


    }
}
﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.RegistrarFactura
{
    public class RegistrarFacturaViewModelValidator : AbstractValidator<RegistrarFacturaViewModel>
    {
        public RegistrarFacturaViewModelValidator()
        {
            RuleFor(modelo => modelo.StrSerie).Must(ValidarSerie).WithMessage("Ingrese Serie");
            RuleFor(modelo => modelo.StrNumero).Must(ValidarNumero).WithMessage("Ingrese Número");
            RuleFor(modelo => modelo.StrRUC).Must(ValidarM11).WithMessage("Ingrese RUC");
            RuleFor(modelo => modelo.DcTotal).Must(ValidarTotal).WithMessage("Ingrese Total");
            RuleFor(modelo => modelo.DcImpuesto).Must(ValidarImpuesto).WithMessage("Ingrese Impuesto");

            RuleFor(modelo => modelo.DtFechaEmision).Must(ValidarFEmision).WithMessage("Ingrese Fecha Correcta");

            RuleFor(modelo => modelo.DtFechaVcto).Must(ValidarFVcto).WithMessage("Ingrese Fecha Correcta");

        }

        private bool ValidarFVcto(RegistrarFacturaViewModel modelo, DateTime Serie)
        {
            if (Serie.Date > DateTime.Now.Date)
                return false;
            if (Serie.Date > modelo.DtFechaCobro.Date) return false;
            return true;
        }


        private bool ValidarFEmision(RegistrarFacturaViewModel modelo,DateTime Serie)
        {
            if (Serie.Date>DateTime.Now.Date)
                return false;
            if (Serie.Date > modelo.DtFechaVcto.Date) return false;
            return true;
        }

        private bool ValidarImpuesto(RegistrarFacturaViewModel modelo, decimal Serie)
        {
            if (Serie <= 0) return false;
            if (((modelo.DcTotal * Convert.ToDecimal(0.18)) - Serie) != 0) return false;
            return true;
        }

        private bool ValidarTotal(decimal Serie)
        {
            if (Serie<= 0 ) return false;            
            return true;
        }

        private bool ValidarSerie(string Serie)
        {
            if (Serie == null) return false;
            if (Serie == string.Empty) return false;
            return true;
        }

        private bool ValidarNumero(string Serie)
        {
            if (Serie == null) return false;
            if (Serie == string.Empty) return false;
            return true;
        }

        private bool ValidarM11(string RUC)
        {
            if (RUC == null) return false;
            if (RUC == string.Empty) return false;
            if (RUC.Substring(0, 1).ToString() != "2")
            {
                return false;
            }

            if (RUC.Length != 11)
            {
                return false;
            }

            int[] intPesos = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            int intAcumulado = 0;
            int intModulo11 = 0;
            for (int i = 0; i <= 9; i++)
            {
                if (char.IsDigit(Convert.ToChar(RUC.Substring(i, 1).ToString())))
                {
                    intAcumulado = intAcumulado + intPesos[i] * Convert.ToInt32(char.GetNumericValue(Convert.ToChar(RUC.Substring(i, 1).ToString())));
                }
                else
                {
                    intAcumulado = -1;
                    break;
                }
            }
            if (intAcumulado > 0)
            {
                intModulo11 = (11 - intAcumulado % 11) % 10;

            }
            if (RUC.Length == 11)
            {
                if (RUC.Substring(10, 1).ToString().Equals(intModulo11.ToString()))
                {
                    return true;
                }
                else
                { return false; }
            }
            else { return false; }

        }



    }


}
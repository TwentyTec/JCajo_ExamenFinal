﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Modelo;
using EFinal.Web.Comun;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.RegistrarFactura
{
    public class RegistrarFacturaHandler : IDisposable
    {
        private readonly FinalRepositorio EmpresaRepositorio;

        public RegistrarFacturaHandler()
        {
            EmpresaRepositorio = new FinalRepositorio(new EFinalContexto());
        }

        public void Dispose()
        {
            EmpresaRepositorio.Dispose();
        }

        public void Ejecutar(RegistrarFacturaViewModel modelo,Imagen imagen)
        {
            Factura ObjNuevaEmpresa = new Factura();
            ObjNuevaEmpresa.StrSerie = modelo.StrSerie;
            ObjNuevaEmpresa.StrNumero = modelo.StrNumero;
            ObjNuevaEmpresa.DcImpuesto = modelo.DcImpuesto;
            ObjNuevaEmpresa.DcTotal = modelo.DcTotal;
            ObjNuevaEmpresa.DtFechaCobro = modelo.DtFechaCobro;
            ObjNuevaEmpresa.DtFechaEmision = modelo.DtFechaEmision;
            ObjNuevaEmpresa.DtFechaVcto = modelo.DtFechaVcto;
            ObjNuevaEmpresa.BitEstado = true;
            ObjNuevaEmpresa.IntIdRegistro = 0;
            ObjNuevaEmpresa.StrNombreArchivoFactura = imagen.Nombre;
            ObjNuevaEmpresa.Empresa = EmpresaRepositorio.Empresas.TraerUno(e => e.StrRUC == modelo.StrRUC);
            EmpresaRepositorio.Facturas.Agregar(ObjNuevaEmpresa);
            EmpresaRepositorio.Commit();

            //imagen.GrabaThumbnail(DateTime.Now.Ticks + Path.GetExtension(imagen.Nombre));
            imagen.GrabaThumbnail(imagen.Nombre);
        }
    }
}
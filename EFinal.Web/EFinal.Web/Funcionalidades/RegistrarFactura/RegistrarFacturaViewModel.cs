﻿using EFinal.Modelo;
using EFinal.Web.Funcionalidades.RegistrarFactura;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.RegistrarFactura
{
    [Validator(typeof(RegistrarFacturaViewModelValidator))]

    public class RegistrarFacturaViewModel
    {
        [Display(Name = "Serie")]
        public string StrSerie { get; set; }
        [Display(Name = "Número")]
        public string StrNumero { get; set; }
        [Display(Name = "Fecha Emisión")]
        public DateTime DtFechaEmision { get; set; }
        [Display(Name = "Fecha Vcto.")]
        public DateTime DtFechaVcto { get; set; }
        [Display(Name = "Fecha Cobro")]
        public DateTime DtFechaCobro { get; set; }
        [Display(Name = "Total")]
        public decimal DcTotal { get; set; }
        [Display(Name = "Impuesto")]
        public decimal DcImpuesto { get; set; }
        [Display(Name = "RUC")]
        public string StrRUC { get; set; }

        [Display(Name = "Razón Social")]
        public string StrRSocial { get; set; }

        public string StrNombreArchivoFactura { get; set; }        
    }
}
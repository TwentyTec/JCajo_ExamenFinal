﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;


namespace EFinal.Web.Funcionalidades.RegistrarEmpresa
{
    public class RegistraEmpresaViewModelValidator : AbstractValidator<RegistrarEmpresaViewModel>
    {
        public RegistraEmpresaViewModelValidator()
        {
            RuleFor(modelo => modelo.StrRUC).Must(ValidarM11).WithMessage("Ingrese RUC valido");
            RuleFor(modelo => modelo.StrRazonSocial).Must(ValidarRSocial).WithMessage("Ingrese R. Social");
        }

        private bool ValidarRSocial(string RSocial)
        {
            if (RSocial == null) return false;
            if (RSocial == string.Empty) return false;
            return true;
        }
        private bool ValidarM11(string RUC)
        {
            if (RUC == null) return false;
            if (RUC == string.Empty) return false;
            if (RUC.Substring(0, 1).ToString() != "2")
            {
                return false;
            }

            if (RUC.Length != 11)
            {
                return false;
            }

            int[] intPesos = {5,4, 3,2,7,6,5,4,3,2};
            int intAcumulado = 0;
            int intModulo11 = 0;
            for (int i = 0; i <= 9; i++)
            {
                if (char.IsDigit(Convert.ToChar( RUC.Substring(i,1).ToString())))
                {
                    intAcumulado = intAcumulado + intPesos[i] * Convert.ToInt32( char.GetNumericValue(Convert.ToChar(RUC.Substring(i, 1).ToString())));
                }
                else
                {
                    intAcumulado = -1;
                    break; 
                }
            }
            if (intAcumulado > 0)
            {
                intModulo11 = (11 - intAcumulado % 11) % 10;

            }
            if (RUC.Length == 11)
            {
                if (RUC.Substring(10, 1).ToString().Equals(intModulo11.ToString()))
                {
                    return true;
                }
                else
                { return false; }
            }
            else { return false; }
            
        }
    }
}
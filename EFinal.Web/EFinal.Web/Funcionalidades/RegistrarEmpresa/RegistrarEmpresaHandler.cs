﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Modelo;
using EFinal.Web.Funcionalidades.RegistrarEmpresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.RegistrarEmpresa
{
    public class RegistrarEmpresaHandler:IDisposable
    {
        private readonly FinalRepositorio EmpresaRepositorio;

        public RegistrarEmpresaHandler()
        {
            EmpresaRepositorio = new FinalRepositorio(new EFinalContexto());
        }

        public void Dispose()
        {
            EmpresaRepositorio.Dispose();
        }

        public void Ejecutar(RegistrarEmpresaViewModel modelo)
        {
            // crear el objeto del modelo dominio Person
            //validaEmailPromotion(modelo);
            //ValidaTipoPersona(modelo);
            //Person persona = CreaPersona(modelo);
            // Ejecutar validaciones de Negocio
            // grabar la persona
            Empresa ObjNuevaEmpresa = new Empresa { StrRUC = modelo.StrRUC, StrDepartamento = modelo.StrDepartamento, StrDireccion = modelo.StrDireccion, StrDistrito = modelo.StrDistrito, StrProvincia = modelo.StrProvincia, StrRazonSocial = modelo.StrRazonSocial, StrRubro = modelo.StrRubro,BitEstado=true,StrUsuarioDuenio=modelo.StrUsuarioDuenio };
            EmpresaRepositorio.Empresas.Agregar(ObjNuevaEmpresa);
            EmpresaRepositorio.Commit();
        }
    }
}
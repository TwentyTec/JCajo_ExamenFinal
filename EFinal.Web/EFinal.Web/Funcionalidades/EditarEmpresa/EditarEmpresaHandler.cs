﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.EditarEmpresa
{
    public class EditarEmpresaHandler: IDisposable
    {

        private readonly FinalRepositorio permisoRepositorio;

        public EditarEmpresaHandler()
        {
            permisoRepositorio = new FinalRepositorio(new EFinalContexto());
        }

        public void Ejecutar(EditarEmpresaViewModel modelo)
        {
            //Empresa ObjParaActualizar = new Empresa { StrRUC=modelo.StrRUC,StrDepartamento=modelo.StrDepartamento, StrDireccion=modelo.StrDireccion, StrDistrito=modelo.StrDistrito,StrProvincia=modelo.StrProvincia,StrRazonSocial=modelo.StrRazonSocial,StrRubro =modelo.StrRubro,BitEstado=true};
            Empresa ObjParaActualizar = permisoRepositorio.Empresas.TraerUno(e => e.StrRUC.Equals(modelo.StrRUC));
            ObjParaActualizar.StrRUC = modelo.StrRUC;
            ObjParaActualizar.StrDepartamento = modelo.StrDepartamento;
            ObjParaActualizar.StrDireccion = modelo.StrDireccion;
            ObjParaActualizar.StrDistrito = modelo.StrDistrito;
            ObjParaActualizar.StrProvincia = modelo.StrProvincia;
            ObjParaActualizar.StrRazonSocial = modelo.StrRazonSocial;
            ObjParaActualizar.StrRubro = modelo.StrRubro;
            ObjParaActualizar.BitEstado = true;

            permisoRepositorio.Empresas.Actualizar(ObjParaActualizar);
            permisoRepositorio.Commit();
        }
        public void Dispose()
        {
            permisoRepositorio.Dispose();
        }

    }
}
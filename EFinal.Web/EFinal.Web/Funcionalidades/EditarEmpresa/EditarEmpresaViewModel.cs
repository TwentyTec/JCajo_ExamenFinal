﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.EditarEmpresa
{
    public class EditarEmpresaViewModel
    {
       [Display(Name = "RUC")]
        public string StrRUC { get; set; }
        [Display(Name = "R.Social")]
        public string StrRazonSocial { get; set; }
        [Display(Name = "Dirección")]

        public string StrDireccion { get; set; }
        [Display(Name = "Departamento")]
        public string StrDepartamento { get; set; }
        [Display(Name = "Provincia")]
        public string StrProvincia { get; set; }
        [Display(Name = "Distrito")]
        public string StrDistrito { get; set; }
        [Display(Name = "Rubro")]
        public string StrRubro { get; set; }

    }
}
﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.EditarEmpresa
{
    public class ObtenerEmpresaHandler : IDisposable
    {
         
        private readonly FinalRepositorio ObjPermiso;

        public ObtenerEmpresaHandler()
        {
            ObjPermiso = new FinalRepositorio(new EFinalContexto());
        }

        public void Dispose()
        {
            ObjPermiso.Dispose();
        }

        public ObtenerEmpresaViewModel Ejecutar(string id)
        {
            var permisoRol = ObjPermiso.Empresas.TraerUno(x => x.StrRUC == id);
            return new ObtenerEmpresaViewModel()
            {
                 StrRUC= permisoRol.StrRUC,
                 StrDepartamento=permisoRol.StrDepartamento,
                 StrDireccion= permisoRol.StrDireccion,
                 StrDistrito=permisoRol.StrDistrito,
                 StrProvincia=permisoRol.StrProvincia,
                 StrRazonSocial=permisoRol.StrRazonSocial,
                 StrRubro=permisoRol.StrRubro              
            };
        }

        public bool ExisteEmpresa(string id)
        {
            var permisoRol = ObjPermiso.Empresas.TraerUno(x => x.StrRUC == id);
            if (permisoRol == null)
            {
                return false;
            }
            else
            {
                return true;
            }           
        }
    }
}
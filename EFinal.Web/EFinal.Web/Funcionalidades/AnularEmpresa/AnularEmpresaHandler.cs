﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.AnularEmpresa
{
    public class AnularEmpresaHandler : IDisposable
    {
        private readonly FinalRepositorio permisoRepositorio;

        public AnularEmpresaHandler()
        {
            permisoRepositorio = new FinalRepositorio(new EFinalContexto());
        }

        public void Ejecutar(string strruc)
        {
            Empresa ObjParaActualizar = new Empresa { StrRUC = strruc, BitEstado = false };
            permisoRepositorio.Empresas.Actualizar(ObjParaActualizar);
            permisoRepositorio.Commit();
        }

        public bool ExisteFacturasEmpresa(string ruc)
        {
            var permisoRol = permisoRepositorio.Facturas.TrerTodos().ToList().Where(e=> e.Empresa.StrRUC.Equals(ruc) && e.BitEstado==true);
            if (permisoRol.ToList().Count() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void Dispose()
        {
            permisoRepositorio.Dispose();
        }
    }
}
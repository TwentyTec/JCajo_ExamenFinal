﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.AnularEmpresa
{
    public class AnularEmpresaViewModel
    {
        [ReadOnly(true)]
        [Display(Name = "RUC")]
        public string StrRUC { get; set; }
        [ReadOnly(true)]
        [Display(Name = "R.Social")]
        public string StrRazonSocial { get; set; }
        [ReadOnly(true)]
        [Display(Name = "Dirección")]

        public string StrDireccion { get; set; }
        [ReadOnly(true)]
        [Display(Name = "Departamento")]
        public string StrDepartamento { get; set; }
        [ReadOnly(true)]
        [Display(Name = "Provincia")]
        public string StrProvincia { get; set; }
        [ReadOnly(true)]
        [Display(Name = "Distrito")]
        public string StrDistrito { get; set; }
        [ReadOnly(true)]
        [Display(Name = "Rubro")]
        public string StrRubro { get; set; }
    }
}
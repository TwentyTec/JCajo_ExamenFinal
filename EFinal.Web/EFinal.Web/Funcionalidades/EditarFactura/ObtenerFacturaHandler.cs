﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Web.Funcionalidades.EditarFactura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.EditarFactura
{
    public class ObtenerFacturaHandler : IDisposable
    {

        private readonly FinalRepositorio ObjPermiso;

        public ObtenerFacturaHandler()
        {
            ObjPermiso = new FinalRepositorio(new EFinalContexto());
        }

        public void Dispose()
        {
            ObjPermiso.Dispose();
        }

        public ObtenerFacturaViewModel Ejecutar(int id)
        {
            var permisoRol = ObjPermiso.Facturas.TraerUno(x => x.IntIdRegistro == id);
            return new ObtenerFacturaViewModel()
            {
               DcImpuesto=permisoRol.DcImpuesto,
                DcTotal = permisoRol.DcTotal,
                DtFechaCobro=permisoRol.DtFechaCobro,
                DtFechaEmision = permisoRol.DtFechaEmision,
                DtFechaVcto = permisoRol.DtFechaVcto,
                IntIdRegistro = permisoRol.IntIdRegistro,
                StrNumero = permisoRol.StrNumero,
                StrRSocial = permisoRol.Empresa.StrRazonSocial,
                StrRUC = permisoRol.Empresa.StrRUC,
                StrSerie = permisoRol.StrSerie,
                StrNombreArchivoFactura = permisoRol.StrNombreArchivoFactura    
            };
        }

        public bool ExisteFacturaSerieNumero(string serie,string numero)
        {
            var permisoRol = ObjPermiso.Facturas.TraerUno(x => x.StrSerie.Equals(serie) && x.StrNumero.Equals(numero));
            if (permisoRol == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
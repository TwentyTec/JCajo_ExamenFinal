﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EFinal.Web.Comun;

namespace EFinal.Web.Funcionalidades.EditarFactura
{
    public class EditarFacturaHandler : IDisposable
    {

        private readonly FinalRepositorio permisoRepositorio;

        public EditarFacturaHandler()
        {
            permisoRepositorio = new FinalRepositorio(new EFinalContexto());
        }

        public void Ejecutar(EditarFacturaViewModel modelo, Imagen imagen)
        {           
            Factura ObjNuevaEmpresa = new Factura();
            ObjNuevaEmpresa.StrSerie = modelo.StrSerie;
            ObjNuevaEmpresa.StrNumero = modelo.StrNumero;
            ObjNuevaEmpresa.DcImpuesto = modelo.DcImpuesto;
            ObjNuevaEmpresa.DcTotal = modelo.DcTotal;
            ObjNuevaEmpresa.DtFechaCobro = modelo.DtFechaCobro;
            ObjNuevaEmpresa.DtFechaEmision = modelo.DtFechaEmision;
            ObjNuevaEmpresa.DtFechaVcto = modelo.DtFechaVcto;
            ObjNuevaEmpresa.BitEstado = true;
            ObjNuevaEmpresa.IntIdRegistro = modelo.IntIdRegistro;
            ObjNuevaEmpresa.StrNombreArchivoFactura = imagen.Nombre;
            ObjNuevaEmpresa.Empresa = permisoRepositorio.Empresas.TraerUno(e => e.StrRUC == modelo.StrRUC);
            permisoRepositorio.Facturas.Actualizar(ObjNuevaEmpresa);
            permisoRepositorio.Commit();

            imagen.GrabaThumbnail(imagen.Nombre);
        }
        public void Dispose()
        {
            permisoRepositorio.Dispose();
        }

    }
}
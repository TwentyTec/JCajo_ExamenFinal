﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.AnularFactura
{
    public class AnularFacturaHandler : IDisposable
    {
        private readonly FinalRepositorio permisoRepositorio;

        public AnularFacturaHandler()
        {
            permisoRepositorio = new FinalRepositorio(new EFinalContexto());
        }

        public void Ejecutar(int id)
        {
            Factura ObjParaActualizar = permisoRepositorio.Facturas.TraerUno(e => e.IntIdRegistro==id);
            ObjParaActualizar.BitEstado = false;
            // new Factura { IntIdRegistro = id, BitEstado = false };
            permisoRepositorio.Facturas.Actualizar(ObjParaActualizar);
            permisoRepositorio.Commit();
        }

        public AnularFacturaViewModel ObtenerFacturaAnular(int id)
        {
            var permisoRol = permisoRepositorio.Facturas.TraerUno(x => x.IntIdRegistro == id);
            return new AnularFacturaViewModel()
            {
                DcImpuesto = permisoRol.DcImpuesto,
                DcTotal = permisoRol.DcTotal,
                DtFechaCobro = permisoRol.DtFechaCobro,
                DtFechaEmision = permisoRol.DtFechaEmision,
                DtFechaVcto = permisoRol.DtFechaVcto,
                IntIdRegistro = permisoRol.IntIdRegistro,
                StrNumero = permisoRol.StrNumero,
                StrRSocial = permisoRol.Empresa.StrRazonSocial,
                StrRUC = permisoRol.Empresa.StrRUC,
                StrSerie = permisoRol.StrSerie                
            };
        }
        public void Dispose()
        {
            permisoRepositorio.Dispose();
        }

    }
}
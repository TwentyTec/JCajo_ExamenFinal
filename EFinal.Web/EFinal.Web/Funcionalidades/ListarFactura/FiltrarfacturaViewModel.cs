﻿using EFinal.Web.Funcionalidades.ListarFactura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.ListarFactura
{
    public class FiltrarFacturaViewModel
    {

        public string Filtro { get; set; }

        public IEnumerable<ListarFacturaViewModel> Facturas { get; set; }

        public FiltrarFacturaViewModel()
        {
            Facturas = new List<ListarFacturaViewModel>();
        }
    }
}
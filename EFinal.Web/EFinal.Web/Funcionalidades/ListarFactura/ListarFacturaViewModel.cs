﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.ListarFactura
{
    public class ListarFacturaViewModel
    {
        [Display(Name = "ID")]
        public int IntIdRegistro { get; set; }
        [Display(Name = "Serie")]
        public string StrSerie { get; set; }
        [Display(Name = "Número")]
        public string StrNumero { get; set; }
        [Display(Name = "Fecha Emisión")]
        public DateTime DtFechaEmision { get; set; }
        [Display(Name = "Fecha Vcto.")]
        public DateTime DtFechaVcto { get; set; }
        [Display(Name = "Fecha Cobro")]
        public DateTime DtFechaCobro { get; set; }
        [Display(Name = "Total")]
        public decimal DcTotal { get; set; }
        [Display(Name = "Impuesto")]
        public decimal DcImpuesto { get; set; }
        [Display(Name = "RUC")]
        public string StrRUC { get; set; }

        [Display(Name = "R.Social")]        
        public string StrRazonSocial { get; set; }

        [Display(Name = "Archivo Digital")]
        public string StrNombreArchivoFactura { get; set; }
    }
}
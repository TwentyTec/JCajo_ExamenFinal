﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Web.Funcionalidades.ListarFactura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.ListarFactura
{
    public class ListarFacturaHandler : IDisposable
    {

        public readonly FinalRepositorio ObjFinalRepositorio;

        public ListarFacturaHandler()
        {
            ObjFinalRepositorio = new FinalRepositorio(new EFinalContexto());

        }

        public void Dispose()
        {
            ObjFinalRepositorio.Dispose();
        }


        public IEnumerable<ListarFacturaViewModel> Ejecutar(string filtro)
        {
            var consulta = ObjFinalRepositorio.Facturas.TrerTodos().Where(e => e.BitEstado == true && ( e.Empresa.StrRUC.Equals(filtro) || e.Empresa.StrUsuarioDuenio.Equals(filtro) ));

            return consulta.Select(e =>
               new ListarFacturaViewModel()
               {
                   IntIdRegistro = e.IntIdRegistro,
                   DcImpuesto = e.DcImpuesto,
                   DcTotal = e.DcTotal,
                   DtFechaCobro = e.DtFechaCobro,
                   DtFechaEmision = e.DtFechaEmision,
                   DtFechaVcto = e.DtFechaVcto,
                   StrSerie = e.StrSerie,
                   StrNumero = e.StrNumero,
                   StrRUC = e.Empresa.StrRUC,
                   StrRazonSocial = e.Empresa.StrRazonSocial,
                   StrNombreArchivoFactura=e.StrNombreArchivoFactura                
               }
                ).ToList();
        }

        public IEnumerable<ListarFacturaViewModel> ListarFacturasPorUsuario(string filtro)
        {
            var consulta = ObjFinalRepositorio.Facturas.TrerTodos().Where(e => e.BitEstado == true && e.Empresa.StrUsuarioDuenio.Equals(filtro));

            return consulta.Select(e =>
               new ListarFacturaViewModel()
               {
                   IntIdRegistro = e.IntIdRegistro,
                   DcImpuesto = e.DcImpuesto,
                   DcTotal = e.DcTotal,
                   DtFechaCobro = e.DtFechaCobro,
                   DtFechaEmision = e.DtFechaEmision,
                   DtFechaVcto = e.DtFechaVcto,
                   StrSerie = e.StrSerie,
                   StrNumero = e.StrNumero,
                   StrRUC = e.Empresa.StrRUC,
                   StrRazonSocial = e.Empresa.StrRazonSocial,
                   StrNombreArchivoFactura = e.StrNombreArchivoFactura
               }
                ).ToList();
        }

    }
}
﻿using EFinal.Aplicacion;
using EFinal.Aplicacion.Impl;
using EFinal.Web.Funcionalidades.ListarEmpresas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.ListarEmpresas
{
    public class ListarEmpresaHandler : IDisposable
    {
        public readonly FinalRepositorio ObjFinalRepositorio;

        public ListarEmpresaHandler()
        {
            ObjFinalRepositorio = new FinalRepositorio(new EFinalContexto());

        }

        public void Dispose()
        {
            ObjFinalRepositorio.Dispose();
        }


        public IEnumerable<ListarEmpresaViewModel> Ejecutar(string filtro)
        {
            var consulta = ObjFinalRepositorio.Empresas.TrerTodos().Where(e=> e.BitEstado==true && e.StrUsuarioDuenio.Equals(filtro));
            //if (!string.IsNullOrEmpty(filtro))
            //{
            //    consulta = consulta.Where(e => e.StrCodRol.Contains(filtro));

            //}


            return consulta.Select(e =>
               new ListarEmpresaViewModel()
               {
                   StrDepartamento=e.StrDepartamento,
                   StrDireccion=e.StrDireccion,
                   StrDistrito=e.StrDistrito,
                   StrProvincia=e.StrProvincia,
                   StrRazonSocial=e.StrRazonSocial,
                   StrRubro=e.StrRubro,
                   StrRUC=e.StrRUC                                 
               }
                ).ToList();
        }



    }
}
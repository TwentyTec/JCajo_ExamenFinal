﻿using EFinal.Web.Funcionalidades.ListarEmpresas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFinal.Web.Funcionalidades.ListarEmpresas
{
    public class FiltrarEmpresasViewModel
    {
        public string Filtro { get; set; }

        public IEnumerable<ListarEmpresaViewModel> Empresas { get; set; }

        public FiltrarEmpresasViewModel()
        {
            Empresas = new List<ListarEmpresaViewModel>();
        }
    }
}
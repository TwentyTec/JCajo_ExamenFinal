﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using EFinal.Modelo;

namespace EFinal.Aplicacion
{
    public class EFinalContexto : DbContext
    {
        public EFinalContexto()
            : base("name=CnExamenFinal")
        {
        }

        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Factura> Factura{ get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           modelBuilder.Entity<Factura>().HasKey(t => new { t.IntIdRegistro });
        }
        }
}

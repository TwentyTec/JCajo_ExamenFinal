﻿using EFinal.Modelo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFinal.Aplicacion.Impl
{
    public class FinalRepositorio : IDisposable
    {

        private readonly DbContext bd;

        private readonly RepositorioGenerico<Empresa> _empresas;
        private readonly RepositorioGenerico<Factura> _facturas;

        public FinalRepositorio(DbContext bd)
        {
            this.bd = bd;
            _empresas = new RepositorioGenerico<Empresa>(bd);
            _facturas = new RepositorioGenerico<Factura>(bd);
        }

        public RepositorioGenerico<Empresa> Empresas { get { return _empresas; } }
        public RepositorioGenerico<Factura> Facturas { get { return _facturas; } }

        public void Commit()
        {
            bd.SaveChanges();
        }

        public void Dispose()
        {
            bd.Dispose();
        }
    }
    }

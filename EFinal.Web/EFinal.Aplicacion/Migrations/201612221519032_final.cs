namespace EFinal.Aplicacion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class final : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        StrRUC = c.String(nullable: false, maxLength: 128),
                        StrRazonSocial = c.String(),
                        StrDireccion = c.String(),
                        StrDepartamento = c.String(),
                        StrProvincia = c.String(),
                        StrDistrito = c.String(),
                        StrRubro = c.String(),
                        BitEstado = c.Boolean(nullable: false),
                        StrUsuarioDuenio = c.String(),
                    })
                .PrimaryKey(t => t.StrRUC);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        IntIdRegistro = c.Int(nullable: false, identity: true),
                        StrSerie = c.String(),
                        StrNumero = c.String(),
                        DtFechaEmision = c.DateTime(nullable: false),
                        DtFechaVcto = c.DateTime(nullable: false),
                        DtFechaCobro = c.DateTime(nullable: false),
                        DcTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DcImpuesto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StrNombreArchivoFactura = c.String(),
                        BitEstado = c.Boolean(nullable: false),
                        Empresa_StrRUC = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IntIdRegistro)
                .ForeignKey("dbo.Empresas", t => t.Empresa_StrRUC)
                .Index(t => t.Empresa_StrRUC);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Facturas", "Empresa_StrRUC", "dbo.Empresas");
            DropIndex("dbo.Facturas", new[] { "Empresa_StrRUC" });
            DropTable("dbo.Facturas");
            DropTable("dbo.Empresas");
        }
    }
}
